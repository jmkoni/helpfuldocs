# FIRST GEM!
require rspec
# good for testing
# well tested, core team, industry standard, good for unit testing
# you must write automated unit tests
# SECOND GEM!
require bourne
# more testing!!!!!
# THIRD GEM
require haml
# html templating library
#FOURTH GEM
require omniauth
# oauth... the future
# industry standard
#FIFTH GEM
require newrelic_rpm
# monitors your site


#SECONDBATCH!
require konacha
# javascript/coffee script testing framework for rails
#SECOND ONE
require sidekiq
# good on heroku, threaded
# asynchronous, uses redis
#THIRD ONE
require sunspot
# powerful search engine, sunspot makes it easy
# supports full text, pagination, etc
# ships with dev version of solr ready to go
#FOURTH!
require sinatra
# dsl for quickly creating web applications
# so pretty! easier than rails, possibly
#FIFTH!
require foreman
# manages application commands


#THIRD ONES!
#first!
require inherited_resources
# abstraction for your controller
# easy api generation
# not for beginners
# second!
require kaminari
# similar to paginate, more object orientated
# easily customizable
# third!
require guard
# good for testing :)
# runs tests whenever you save
# manages test runs
# fourth!
require configatron
# configures credentials
# fifth!
require bourbon
# gives set of macros, makes your page look beautiful
# bourbon.io

#FOURTH!!!!
#first one
require zeus
# like guard, helps you do things faster and more efficiently
# gem install zeus, don't put it in gemfile!
#second one
require active_hash
# read only data source - for stuff that's not really data
#third one
require simple_form
# creates html markup for you
#fourth one
require redcarpet
# renders markdown
# great with haml
#fifth one
require better_errors
# only add to your gemfile development environment
# super cool error display
