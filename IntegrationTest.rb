# capybara gem - allows to intereact with webpage
# gist.github.com/drapergeek/4718737
# mkdir spec/features/file_name_here_spec.rb
require spec_helper

feature 'user signs up' do
	scenario 'without errors' do
		School.create(name: 'Boston High School')
		visit '/'
		click_link 'Begin Application Process' # looks for exact text on page and clicks on it
# 		save_and_open_page #from capybara
		click_link 'Continue'
		click_link 'Continue'
		click_link 'Begin Application'
		
		# start filling out form
		fill_in 'First Name', with: 'Peter' # text input field
		choose "Male" # radio button
		check 'Teachers Contacted' # check box
		
		#currently the school has no data_type (created above)
		select 'Boston High School', from: 'School'
		click button 'Submit Information'
		page.should_have_content("Application successfully submitted")
		# use wait_until if you need to wait for the page to do something
	end
end